package vn.silverlining.app;

import vn.silverlining.base.BaseActivity;
import vn.silverlining.whereonearth.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LoginActivity extends BaseActivity implements OnClickListener {
	private Button btnLogin;

	@Override
	public void updateView() {
		
	}

	@Override
	public void loadData() {
		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(this);
	}

	@Override
	public void loadControls(Bundle savedInstanceState) {
		
	}
	
	@Override
	public int getLayoutResource() {
		return R.layout.activity_login;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.btnLogin) {
			
		}
	}

}
