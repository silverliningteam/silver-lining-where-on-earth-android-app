package vn.silverlining.base;

import vn.silverlining.whereonearth.R;
import android.app.Activity;
import android.os.Bundle;

public abstract class BaseActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initActivity(savedInstanceState);
	}

	protected void initActivity(Bundle savedInstanceState) {
		setContentView(getLayoutResource());

		loadControls(savedInstanceState);

		loadData();

		updateView();
	}
	
	public abstract void updateView();

	public abstract void loadData();

	public abstract void loadControls(Bundle savedInstanceState);

	public int getLayoutResource() {
		return R.layout.transparent_layout;
	}
}
